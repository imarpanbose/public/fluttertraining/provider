import 'dart:js';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_example/provider/app_provider.dart';
import 'package:provider_example/provider/product_provider.dart';
import 'package:provider_example/view/PageOne.dart';

void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (context)=>AppProvider()),
    ChangeNotifierProvider(create: (context)=>ProductProvider()),
  ],
  child: const MainApp(),));
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: PageOne()
      ),
    );
  }
}
