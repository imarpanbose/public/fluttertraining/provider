import 'package:flutter/material.dart';

class AppProvider extends ChangeNotifier{
  int count = 0;

  increase(){
    count+=1;
    notifyListeners();
  }
}