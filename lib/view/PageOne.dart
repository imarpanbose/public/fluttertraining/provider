import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_example/provider/app_provider.dart';
import 'package:provider_example/view/PageTwo.dart';

class PageOne extends StatefulWidget {
  PageOne({super.key});

  @override
  State<PageOne> createState() => _PageOneState();
}

class _PageOneState extends State<PageOne> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("PageOne"),),
      body: ListView(
        children: [
          SizedBox(height: 200),
          Center(child:Consumer<AppProvider>(builder: (context,value,_)=>Text('${value.count}',style: TextStyle(fontSize: 30),),) ,),
          Padding(padding: EdgeInsets.all(60),
          child: ElevatedButton(onPressed: () =>Provider.of<AppProvider>(context,listen: false).increase(),
          child: Text("Increase"),),)
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=> PageTwo())),
        tooltip: "Go to PageTwo",
        child: Icon(Icons.arrow_right),
        ),
    );
  }
}