import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_example/provider/app_provider.dart';

class PageTwo extends StatefulWidget {
  PageTwo({super.key});

  @override
  State<PageTwo> createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("PageTwo"),),
      body: ListView(
        children: [
          SizedBox(height: 200),
          Center(child:Consumer<AppProvider>(builder: (context,value,_)=>
          Text('${(value.count)}',style: TextStyle(fontSize: 30),),),),
          Padding(padding: EdgeInsets.all(60),
          child: ElevatedButton(onPressed: () =>Provider.of<AppProvider>(context,listen: false).increase(),
          child: Text("Increase"),),)
        ],
      ),
    );
  }
}